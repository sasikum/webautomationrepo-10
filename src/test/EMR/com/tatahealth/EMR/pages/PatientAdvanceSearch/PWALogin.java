package com.tatahealth.EMR.pages.PatientAdvanceSearch;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class PWALogin {
	public WebElement getLoginBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement lgnbtn = Web_GeneralFunctions.findElementbyXPath("//div[contains(text(),'login')]",driver,logger);
		return lgnbtn;	
	}
	
	public WebElement getMobileNumber(WebDriver driver,ExtentTest logger) 
	{
		WebElement num = Web_GeneralFunctions.findElementbyXPath("//input[@aria-label='mobile-number']",driver,logger);
		return num;	
	}
	
	public WebElement getContinueBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement signinbtn = Web_GeneralFunctions.findElementbyXPath("//button[contains(text(),'Continue')]",driver,logger);
		return signinbtn;	
	}
	
	public WebElement getOTP(int num,WebDriver driver,ExtentTest logger) {
		WebElement otp1=Web_GeneralFunctions.findElementbySelector("input#otpPin"+num, driver, logger);
		return otp1;
	}
	
	
	public WebElement getName(WebDriver driver,ExtentTest logger) {
		WebElement name=Web_GeneralFunctions.findElementbySelector("div[class*='profiletextfield'] input[placeholder*='name']", driver, logger);
		return name;
	}
	
	public WebElement getAge(WebDriver driver,ExtentTest logger) {
		WebElement age=Web_GeneralFunctions.findElementbySelector("div[class*='agewraper'] input[placeholder*='Age']", driver, logger);
		return age;
	}
	
	public WebElement getGender(WebDriver driver,ExtentTest logger) {
		WebElement gender=Web_GeneralFunctions.findElementbyXPath("//div[text()='Male']", driver, logger);
		return gender;
		
	}
	
	public WebElement getEmail(WebDriver driver,ExtentTest logger) {
		WebElement email=Web_GeneralFunctions.findElementbyXPath("input[placeholder*='email']", driver, logger);
		return email;
		
	}
	
	public WebElement getSignedUpUserBtn(String name,WebDriver driver,ExtentTest logger) 
	{
		WebElement lgnbtn = Web_GeneralFunctions.findElementbyXPath("//div[contains(text(),'"+name+"')]",driver,logger);
		return lgnbtn;	
	}
	
	public WebElement myAccountBtn(WebDriver driver,ExtentTest logger)
	{
		WebElement mycnt=Web_GeneralFunctions.findElementbyXPath("//div[contains(text(),'My Account')]", driver, logger);
		return mycnt;
	}
	
	public WebElement mySettingBtn(WebDriver driver,ExtentTest logger)
	{
		WebElement settings=Web_GeneralFunctions.findElementbyXPath("//p[contains(text(),'Settings')]", driver, logger);
		return settings;
	}
	
	
	public WebElement logoutBtn(WebDriver driver,ExtentTest logger)
	{
		WebElement logout=Web_GeneralFunctions.findElementbyXPath("//button[text()='Logout']", driver, logger);
		return logout;
	}
	public WebElement confirmLogout(WebDriver driver,ExtentTest logger)
		{
			WebElement logout=Web_GeneralFunctions.findElementbyXPath("//button[text()='LOGOUT']", driver, logger);
			return logout;
	}
	
	public WebElement getHomeBtn(WebDriver driver,ExtentTest logger)
	{
		WebElement elm=Web_GeneralFunctions.findElementbyXPath("//div[text()='HOME']", driver, logger);
		return elm;
	}
}
