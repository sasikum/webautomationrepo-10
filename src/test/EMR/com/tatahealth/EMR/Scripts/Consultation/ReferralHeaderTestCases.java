		
package com.tatahealth.EMR.Scripts.Consultation;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.ConsultationPage;
import com.tatahealth.EMR.pages.Consultation.AfterCheckedOutPages;
import com.tatahealth.EMR.pages.Consultation.PrescriptionPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ReferralHeaderTestCases {
	
	ExtentTest logger;
	
	@BeforeClass(groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		//Login_Doctor.userName = "Deiva Stg"; // wy this user name AND password ?
		//Login_Doctor.password = "***";
		//Login_Doctor.userName = "SonalinSTGEMR"; 
		//Login_Doctor.password = "****";
		//Login_Doctor.userName = "AutomationStg"; 
		//Login_Doctor.password = "Test@1234";
		//Login_Doctor.LoginTest(); // wy logging again
		Login_Doctor.LoginTestwithDiffrentUser("AutomationOne");
		System.out.println("Initialized Login_Doctor.driver");
		
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Login_Doctor.driver");
		Reports.extent.flush();
	}

	@Test(groups= {"Regression","Login"},priority=868)
	public synchronized void verifyReferralHeader()throws Exception{
		ConsultationTest consult = new ConsultationTest();
		logger = Reports.extent.createTest("EMR verify referral header");
		AfterCheckedOutPages pages = new AfterCheckedOutPages();
		
		//slotId and prevSlotId is used for starting the same consultation.
		//for this test we can not and should not re-use the same consultation
		//we need to start a new consultation with the referral doctor.
		//hence saving those values in a temp variable and reassigning them after tests are over
		String tempSlotId = ConsultationTest.slotId;
		String tempPrevSlotId = ConsultationTest.prevSlotId;
		ConsultationTest.slotId = "";
		ConsultationTest.prevSlotId = "";
		consult.startConsultation();
		
		String text = "";
		String value = "";
		//referral header
		if(pages.getReferralHeader(Login_Doctor.driver, logger)) {
			
			//referred by
			value += "Dr. "+ReferralTest.loggedInDoctor +" on";
			text = Web_GeneralFunctions.getText(pages.getReferralDivElementHeaders(1, Login_Doctor.driver, logger), "get referral headers", Login_Doctor.driver, logger);
			System.out.println("refere by text : "+text);
			if(text.equalsIgnoreCase("Referred by")) {
				if(value.contains(value)) {
					assertTrue(true);
				}else {
					assertTrue(false);
				}
			}else {
				assertTrue(false);
			}
			
			
			//diagnosis
			text = Web_GeneralFunctions.getText(pages.getReferralDivElementHeaders(2, Login_Doctor.driver, logger), "get referral headers", Login_Doctor.driver, logger);
			value = Web_GeneralFunctions.getAttribute(pages.getReferralDivElementValues(2, Login_Doctor.driver, logger), "title", "get referral elements", Login_Doctor.driver, logger);
			System.out.println("refere by diag text : "+text + " "+value);
			if(text.equalsIgnoreCase("Diagnosis")) {
				if(!value.isEmpty()) {
					assertTrue(true);
				}else {
					assertTrue(false);
				}
			}else {
				assertTrue(false);
			}
			
			//notes section
			text = Web_GeneralFunctions.getText(pages.getReferralDivElementHeaders(3, Login_Doctor.driver, logger), "get referral headers", Login_Doctor.driver, logger);
			value = Web_GeneralFunctions.getAttribute(pages.getReferralDivElementValues(3, Login_Doctor.driver, logger), "title", "get referral elements", Login_Doctor.driver, logger);
			System.out.println("refere by notes text : "+text + " "+value);
			if(text.equalsIgnoreCase("Notes")) {
				if(value.equalsIgnoreCase(ReferralTest.comment)) {
					assertTrue(true);
				}else {
					assertTrue(false);
				}
			}else {
				assertTrue(false);
			}
				
			Web_GeneralFunctions.click(pages.getDismissElement(Login_Doctor.driver, logger), "click dismiss element", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(1);
		}else{
			assertTrue(false);
		}
		ConsultationTest.slotId = tempSlotId;
		ConsultationTest.prevSlotId = tempPrevSlotId;
	}
	
	/*
	 * @Test(groups= {"Regression","Login"},priority=1301) public synchronized void
	 * CIMSInteractionYes()throws Exception{ logger =
	 * Reports.extent.createTest("EMR cims interaction"); PrescriptionPage pPage =
	 * new PrescriptionPage(); PrescriptionTest pTest = new PrescriptionTest();
	 * pTest.moveToPrescriptionModule(); pTest.selectClinicSpecificMedicine();
	 * pTest.selectTDHMedicine();
	 * 
	 * String interactionMedicine = "CARBADAC 200 TAB 200mg"; PrescriptionTest.row =
	 * 0;
	 * 
	 * Web_GeneralFunctions.click(pPage.getDrugName(0, Login_Doctor.driver, logger),
	 * "click drug name", Login_Doctor.driver, logger); Thread.sleep(1000);
	 * 
	 * Web_GeneralFunctions.click(pPage.getFirstMedicineFromDropDown(
	 * PrescriptionTest.row, interactionMedicine, Login_Doctor.driver, logger),
	 * "Get 1st medicine", Login_Doctor.driver, logger); Thread.sleep(3000);
	 * pTest.enterFrequency(); pTest.enterDuration(); pTest.addMedicine();
	 * 
	 * interactionMedicine = "CROCIN ADVANCE TAB 500mg"; PrescriptionTest.row++;
	 * 
	 * Web_GeneralFunctions.click(pPage.getFirstMedicineFromDropDown(
	 * PrescriptionTest.row, interactionMedicine, Login_Doctor.driver, logger),
	 * "Get 1st medicine", Login_Doctor.driver, logger); Thread.sleep(3000);
	 * 
	 * Web_GeneralFunctions.click(pPage.getYesInInteractionPopUp(Login_Doctor.
	 * driver, logger), "click Yes button in interaction pop up",
	 * Login_Doctor.driver, logger); Thread.sleep(2000); //Prescrip
	 * pTest.enterFrequency(); pTest.enterDuration(); pTest.saveMedicine();
	 * pTest.moveToPrescriptionModule(); String text =
	 * Web_GeneralFunctions.getAttribute(pPage.getDrugName(PrescriptionTest.row,
	 * Login_Doctor.driver, logger), "value", "Get drug name value",
	 * Login_Doctor.driver, logger); Thread.sleep(1000);
	 * if(text.equalsIgnoreCase(interactionMedicine)) { assertTrue(true); }else {
	 * assertTrue(false); }
	 * 
	 * }
	 * 
	 * @Test(groups= {"Regression","Login"},priority=1302) public synchronized void
	 * medicineDuplicateBox()throws Exception{ logger =
	 * Reports.extent.createTest("EMR duplicate box"); PrescriptionPage pPage = new
	 * PrescriptionPage(); PrescriptionTest pTest = new PrescriptionTest();
	 * PrescriptionTest.row=2;
	 * 
	 * Web_GeneralFunctions.click(pPage.getFirstMedicineFromDropDown(
	 * PrescriptionTest.row, "", Login_Doctor.driver, logger), "Get 1st medicine",
	 * Login_Doctor.driver, logger); Thread.sleep(3000); pTest.enterFrequency();
	 * pTest.enterDuration(); pTest.addMedicine();
	 * 
	 * PrescriptionTest.row++;
	 * 
	 * Web_GeneralFunctions.click(pPage.getFirstMedicineFromDropDown(
	 * PrescriptionTest.row, "", Login_Doctor.driver, logger), "Get 1st medicine",
	 * Login_Doctor.driver, logger); Thread.sleep(3000);
	 * 
	 * Web_GeneralFunctions.click(pPage.getYesButtonInDuplicateBox(Login_Doctor.
	 * driver, logger), "click yes oin duplicate box", Login_Doctor.driver, logger);
	 * Thread.sleep(1000); pTest.enterFrequency(); pTest.enterDuration();
	 * 
	 * pTest.saveMedicine();
	 * 
	 * 
	 * }
	 */
}
