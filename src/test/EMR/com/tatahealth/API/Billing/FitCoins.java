package com.tatahealth.API.Billing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
//import org.json.JSONArray;
import org.json.JSONObject;
import org.xml.sax.SAXException;

import com.tatahealth.API.Core.CommonFunctions;

public class FitCoins extends Login {

	public void getFitCoins(int money) throws ClientProtocolException, JSONException, IOException, SAXException, ParserConfigurationException{
		url = baseurl + "/rewards/v1/add-reward-points";
		int points = money*20;
	    Calendar calendar = Calendar.getInstance();
	    calendar.add(Calendar.DAY_OF_YEAR, 2);
	    Date expiryDate = calendar.getTime();
	    long expiry = expiryDate.getTime();
	    
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));
		
		JSONObject param = new JSONObject();
		param.put("consumerUserId", consumerUserId);
		param.put("moduleType", "OnBoarding");
		param.put("orderId", 0);
		param.put("points", points);
		param.put("description", "reports_automation");
		param.put("sendNotification", true);
		param.put("expiryDate", expiry);
		
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		message = responseObj.getJSONObject("status").getString("message");
	}
	
	/*public void getFitCoinDetailsOfUser() throws Exception{
		
		
	}*/
	
/*	public JSONArray vitalArray = null;
	public JSONArray medDtlArray = null;
	public void getVitalDetailsOfConsumer() throws Exception{
		vitalArray = new JSONArray();
		url = environment + "/consumerOnboarding/api/v1/medical/getMedicalDetailsVitals";
		
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));
		
		JSONObject param = new JSONObject();
		param.put("consumerUserId", consumerUserId);
		
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		
		message = responseObj.getJSONObject("status").getString("message");
		JSONArray obj= responseObj.getJSONObject("responseData").getJSONArray("vitals");
		//Integer temperature = 0;
		JSONObject vitalObj = null;
		
		JSONObject tempObj = null;
		for(int i=0;i<obj.length();i++) {
			
			vitalObj = (JSONObject) obj.get(i);
			tempObj = new JSONObject();
			if(vitalObj.getString("field").equalsIgnoreCase("temperature")) {
				tempObj.put("field", vitalObj.getString("field"));
				tempObj.put("value", 100);
			}else if(vitalObj.getString("field").equalsIgnoreCase("weight")){
				tempObj.put("field", vitalObj.getString("field"));
				tempObj.put("value", 50);
			}
			else {
				tempObj.put("field", vitalObj.getString("field"));
				tempObj.put("value", vitalObj.getInt("value"));
			}

			
			vitalArray.put(tempObj);
			
		}
		//System.out.println("get medical details : "+message);
		//System.out.println("vital array : "+vitalArray);
		
	}
	
	
	public void updateVitals() throws Exception{
		getVitalDetailsOfConsumer();
		url = environment + "/consumerOnboarding/api/v1/medical/editMedicalDetailsVitals";
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));
		JSONObject param = new JSONObject();
		param.put("consumerUserId", consumerUserId);
		param.put("medicalDetails", vitalArray);
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		message = responseObj.getJSONObject("status").getString("message");
		//System.out.println("vital edit message : "+message);

	}
	
public void getMedicalDetailsOfConsumer() throws Exception{
	medDtlArray = new JSONArray();
		url = environment + "/consumerOnboarding/api/v1/medical/getMedicalData";
		
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));
		
		JSONObject param = new JSONObject();
		param.put("consumerUserId", consumerUserId);
		
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		
		message = responseObj.getJSONObject("status").getString("message");
		JSONArray obj= responseObj.getJSONObject("responseData").getJSONArray("medicalData");
		//Integer temperature = 0;
		JSONObject medDtlObj = null;
		System.out.println(obj);
		JSONObject tempObj = null;
		for(int i=0;i<obj.length();i++) {
			
			medDtlObj = (JSONObject) obj.get(i);
			tempObj = new JSONObject();
			if(medDtlObj.getString("field").equalsIgnoreCase("waist_circumference")) {
				tempObj.put("field", medDtlObj.getString("field"));
				tempObj.put("value", 20);
			}
			else {
				tempObj.put("field", medDtlObj.getString("field"));
				tempObj.put("value", medDtlObj.getInt("value"));
			}
			medDtlArray.put(tempObj);
			
		}
		//System.out.println("get medical details : "+message);
		//System.out.println("vital array : "+vitalArray);
		
	}
	public void updateMedicalDetails() throws Exception{
		getMedicalDetailsOfConsumer();
		url = environment + "/consumerOnboarding/api/v1/medical/saveOrUpdateMedicalData";
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));
		JSONObject param = new JSONObject();
		param.put("consumerUserId", consumerUserId);
		param.put("medicalData", medDtlArray);
		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		message = responseObj.getJSONObject("status").getString("message");
		System.out.println("Medical Detail message : "+message);
	}*/
}
