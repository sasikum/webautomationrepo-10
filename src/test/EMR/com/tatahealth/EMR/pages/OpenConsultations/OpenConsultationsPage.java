package com.tatahealth.EMR.pages.OpenConsultations;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class OpenConsultationsPage {
	

	public WebElement getOpenConsultationsSideBarLink(WebDriver driver,ExtentTest logger) {
		String xpath ="//li[@name='openConsultation']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getPatientNameSearchField(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@name='patientName']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}

	public WebElement getMobileNumberSearchField(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@name='mobileNumber']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getStartDateField(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@name='startDate']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getEndDateField(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@name='endDate']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getPatientIdField(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@name='patientId']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getSearchButton(WebDriver driver,ExtentTest logger) {
		String xpath ="//button[@id='search']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getNoOpenConsultationsText(WebDriver driver,ExtentTest logger) {
		String xpath ="//center[contains(text(),'No Open Consultation Found.')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public List<WebElement> getListOfOpenConsultationRowElements(WebDriver driver,ExtentTest logger){
		String xpath ="//table[@id='openConsultationTable']//tbody//tr";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public  WebElement getFirstOpenConsultationViewButton(WebDriver driver,ExtentTest logger){
		String xpath ="(//table[@id='openConsultationTable']//tbody//tr//td[7]//a)[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public  WebElement getFirstOpenConsultationCheckoutButton(WebDriver driver,ExtentTest logger){
		String xpath ="(//table[@id='openConsultationTable']//tbody//tr//td[7]//a)[2]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public  WebElement getNoInCloseConsultationModal(WebDriver driver,ExtentTest logger){
		String xpath ="//button[@class='cancel']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public List<WebElement> getAllDiagnosisRowDeleteElements(WebDriver driver,ExtentTest logger){
		String xpath ="//table[@id='diagnosisNewTable']//tbody//td//button";
		return Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
	}
	
	public List<WebElement> getAllPrescriptionRowDeleteElements(WebDriver driver,ExtentTest logger){
		String xpath ="//table[@id='medicine_table']//tbody/tr/th/button";
		return Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
	}

public WebElement getPrescriptionHeaderInConsultationPage(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[@id='prescriptionMasterFormContainer']/h4[contains(text(),'Prescription')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}

	public  WebElement getYesInCloseConsultationModal(WebDriver driver,ExtentTest logger){
		String xpath ="//button[@class='confirm']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public List<WebElement> getAllCheckoutElements(WebDriver driver,ExtentTest logger){
		String xpath ="//a[contains(text(),'Check out')]";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}

	public List<WebElement> getActiveFollowUpsOnAppointmentDashboard(WebDriver driver,ExtentTest logger){
		String xpath ="//ul[@class='appointmentwarp']//li//div[contains(text(),'Follow up')]";
		return(Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}

	public  WebElement getPatientDetailsElement(WebDriver driver,ExtentTest logger){
		String xpath ="//table[@id='openConsultationTable']//tbody//td[2]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public  WebElement getPatientMobileNumberElement(WebDriver driver,ExtentTest logger){
		String xpath ="//table[@id='openConsultationTable']//tbody//td[3]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getMobileNumberPopOverMessage(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[@class='popover-content']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getMonthPicker(WebDriver driver,ExtentTest logger) {
		String xpath ="//select[@data-handler='selectMonth']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getYearPicker(WebDriver driver,ExtentTest logger) {
		String xpath ="//select[@data-handler='selectYear']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}

	public WebElement getNextDatePicker(WebDriver driver,ExtentTest logger) {
		String xpath ="//a[@title='Next']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getFutureDayElement(WebDriver driver,int day,ExtentTest logger) {
		String xpath ="(//span[@class='ui-state-default'][contains(text(),'"+day+"')])[1]//parent::td";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}

	public List<WebElement> getAllFutureDayElements(WebDriver driver,ExtentTest logger){
		String xpath ="//span[@class='ui-state-default']";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}


	
	public List<String> getAllYearOptionsAsList(WebDriver driver,ExtentTest logger) throws Exception{
		List<String> displayedYears = new ArrayList<>();
		String xpath ="//select[@class='ui-datepicker-year']//option";
		List<WebElement> allYearElements =Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		for(WebElement yearElement :allYearElements) {
			displayedYears.add(Web_GeneralFunctions.getText(yearElement, "extract year value", Login_Doctor.driver, logger));
		}
		return displayedYears;
		
	}
	
	public int getWindowHandlesSize(WebDriver driver,ExtentTest logger) {
		return(driver.getWindowHandles().size());
		
	}
	
	public WebElement getDateText(WebDriver driver,ExtentTest logger) {
		String xpath ="//table[@id='openConsultationTable']//td[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getBillingHeader(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[contains(text(),'Billing')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getCreateBillHeader(WebDriver driver,ExtentTest logger) {
		String xpath ="//a[contains(text(),'Create Bill')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}

	public WebElement getConsultationStatus(WebDriver driver,ExtentTest logger) {
		String xpath ="//table[@id='openConsultationTable']//td[6]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getCloseConsultationConfirmationPopup(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[@class='sweet-alert showSweetAlert visible']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getCloseConfirmationPopupText(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[@class='sweet-alert showSweetAlert visible']//p[contains(text(),'Do you want to close consultation?')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public List<WebElement> getAllOpenConsultationPatientColumnValuesAsList(WebDriver driver,ExtentTest logger) {
		
		String xpath ="//table[@id='openConsultationTable']//tbody//tr//td[2]";
		List<WebElement> patientNames = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		return patientNames;
	}
	
public List<WebElement> getAllOpenConsultationStatusColumnValuesAsList(WebDriver driver,ExtentTest logger) {
		
		String xpath ="//table[@id='openConsultationTable']//tbody//tr//td[6]";
		List<WebElement> patientNames = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		return patientNames;
	}
	
	
	public WebElement getConsultingAppointmentViewButton(WebDriver driver,ExtentTest logger) {
		String xpath ="(//td[@title='Consulting']//parent::tr//td[7]//a)[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getFollowUpAppointmentViewButton(WebDriver driver,ExtentTest logger) {
		String xpath ="(//td[@title='Follow up']//parent::tr//td[7]//a)[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getFollowUpAppointmentCheckoutButton(WebDriver driver,ExtentTest logger) {
		String xpath ="(//td[@title='Follow up']//parent::tr//td[7]//a)[2]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getConsultingAppointmentCheckoutButton(WebDriver driver,ExtentTest logger) {
		String xpath ="(//td[@title='Consulting']//parent::tr//td[7]//a)[2]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getFollowupLablel(WebDriver driver,ExtentTest logger) {
		String xpath ="//h4[contains(text(),'Follow Up')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getFollowupCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@id='consultationFollowUpCheck']//parent::label//span";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getFollowupInputField(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@id='consultationFollowUpInput']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getCaseSheetNotesElement(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[@class='examinationCasesheet']//label[contains(text(),'Notes')]//parent::div//textarea";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public List<WebElement> getAllPatientVisitElements(WebDriver driver,ExtentTest logger){
		String xpath ="//div[@class='patientvisithistory']//child::a";
		return Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
	}
	
	public WebElement getLatestPatientVisit(WebDriver driver,ExtentTest logger) {
		
		return getAllPatientVisitElements(driver,logger).get(0);
	}
	
	public WebElement getTimeLineSummaryModal(WebDriver driver, ExtentTest logger) {
		String xpath ="//div[@id='summaryContent']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getSymptomInTimeLineSummary(WebDriver driver,ExtentTest logger) {
		String xpath ="//label[contains(text(),' Complaints ')]//ancestor::tr//td[2]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getDiagnosisInTimeLineSummary(WebDriver driver,ExtentTest logger) {
		String xpath ="//label[contains(text(),' Diagnosis ')]//ancestor::tr//td[2]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getMedicineInTimeLineSummary(WebDriver driver,ExtentTest logger) {
		String xpath ="(//table[@id='prescriptionSummaryList']//tbody//tr//td//p)[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getLabTestInTimeLineSummary(WebDriver driver,ExtentTest logger) {
		String xpath ="//label[contains(text(),' Lab Test ')]//ancestor::tr//td[2]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getGeneralAdviceInTimeLineSummary(WebDriver driver,ExtentTest logger) {
		String xpath ="//label[contains(text(),' General Advice ')]//ancestor::tr//td[2]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getFollowUpInTimeLineSummary(WebDriver driver,ExtentTest logger) {
		String xpath ="//label[contains(text(),' Follow Up ')]//ancestor::tr//td[2]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getCaseSheetTextInTimeLineSummary(WebDriver driver,ExtentTest logger) {
		String xpath ="//table[@class='table table-bordered summary-examination-list']//tbody//tr//p";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getPrintSummaryButton(WebDriver driver,ExtentTest logger) {
		String xpath ="//button[contains(text(),'Print Summary PDF')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getPrintAllButton(WebDriver driver,ExtentTest logger) {
		String xpath ="//button[contains(text(),'Print All')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getTimeLineSummaryCloseButton(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[@id='patienttimelineModal']//button[@class='close']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getSecondAppointmentCheckoutButton(WebDriver driver,ExtentTest logger) {
		String xpath ="(//td[@title='Consulting']//parent::tr//td[7])[2]//a[contains(text(),'Check out')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getLatestTimeLine(WebDriver driver,ExtentTest logger) {
		String xpath ="(//div[@id='myCarouselpatientvisit']//div//a)[2]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	
	}
	
	public List<WebElement> getAllTimeLineElements(WebDriver driver,ExtentTest logger){
		String xpath ="//div[@id='myCarouselpatientvisit']//div//a";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	
	public WebElement getFollowupDate(WebDriver driver,ExtentTest logger) {
		String xpath ="//span[@id='consultationFollowUpDate']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	
	}
	
	public WebElement getResendOTP(WebDriver driver,ExtentTest logger) {
		String xpath ="//p[contains(text(),'Resend OTP')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	//consumer app page object
	public WebElement getMobielNoTextField(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@placeholder='Enter Mobile Number']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getContinueButton(WebDriver driver,ExtentTest logger) {
		String xpath ="//button[contains(text(),'Continue')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getEnterOTPHeader(WebDriver driver,ExtentTest logger) {
		String xpath ="//h2[contains(text(),'Enter OTP')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getOtpTextField(WebDriver driver,ExtentTest logger) {
		String xpath ="(//input[@type='TEL'])[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getHealthLocker(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[contains(text(),'HEALTH LOCKER')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getHealthLockerPinField(WebDriver driver,ExtentTest logger) {
		String xpath ="(//input[@type='tel'])[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getConsultationAndPrescription(WebDriver driver,ExtentTest logger) {
		String xpath ="//h4[contains(text(),'Consultation & Prescription')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getPlusFloatingButton(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[@id='floating-button']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getFirstConsultationRow(WebDriver driver,ExtentTest logger) {
		String xpath ="(//h4[contains(text(),'Dr')])[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getPdfFileButton(WebDriver driver,ExtentTest logger) {
		String xpath ="(//button//img[@alt='File Type'])[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	public WebElement getNewWindow(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbySelector("body", driver, logger));
	}
	
	public WebElement getLogin(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[contains(text(),'login')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public List<WebElement> getPdfFilesAsList(WebDriver driver,ExtentTest logger) {
		String xpath ="//button//img[@alt='File Type']";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public WebElement getNotificationElement(WebDriver driver,ExtentTest logger) {
		String xpath ="//img[@alt='Notification']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getNotificationBell(WebDriver driver,ExtentTest logger) {
		String xpath ="(//span[@role='status'])[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	public WebElement getLatestLabTestNotfication(WebDriver driver, ExtentTest logger) {
		String xpath ="(//h4[contains(text(),'Lab Tests')]//ancestor::button)[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getLatestFeedbackNotification(WebDriver driver,ExtentTest logger) {
		String xpath ="(//h4[contains(text(),'Feedback for your Consultation')]//ancestor::button)[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
		
	}
	
	public WebElement getLatestMedicineNotification(WebDriver driver,ExtentTest logger) {
		String xpath ="(//h4[contains(text(),'Prescription')]//ancestor::button)[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
		
	}
	
	public WebElement getLatestReminderNotification(WebDriver driver,ExtentTest logger) {
		String xpath ="(//h4[contains(text(),'Add Reminder')]//ancestor::button)[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
		
	}
	
	public WebElement getLatestAppointmentNotification(WebDriver driver,ExtentTest logger) {
		String xpath ="//h4[contains(text(),'Appointment Confirmation')]//ancestor::button)[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
		
	}
	
	public WebElement getSessionExpireModal(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[contains(text(),'Your SeCure session is expired')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getOkInSessionExpireModal(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[contains(text(),'OK')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	//register patient on all slots page
	
	public WebElement getRegisterPatientButton(String slotid, WebDriver driver,ExtentTest logger) {
		String xpath ="//button[@id='registerpatientbtn"+slotid+"']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger)); 
	}
	
	public WebElement getInPersonConsultationButton(String slotid,WebDriver driver,ExtentTest logger) {
		String xpath ="//div[@id='registerpatientcontainer"+slotid+"']//button[contains(text(),' In-person consultation ')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger)); 
	}
	
	public WebElement getPatientId(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[@class='appointmentdoctorname']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getExaminationInConsultationPage(WebDriver driver,ExtentTest logger) {
		String xpath ="//li[@data-input='Examination']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getPatientTimeLine(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[@class='patientvisithistory']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public List<WebElement> getActiveConsultationsOnAppointmentDashboard(WebDriver driver,ExtentTest logger){
		String xpath ="//ul[@class='appointmentwarp']//li//div[contains(text(),'Consulting')]";
		return(Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
}
