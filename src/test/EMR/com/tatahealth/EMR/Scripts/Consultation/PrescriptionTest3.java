
package com.tatahealth.EMR.Scripts.Consultation;

import static org.testng.Assert.assertTrue;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.DiagnosisPage;
import com.tatahealth.EMR.pages.Consultation.FollowUpPage;
import com.tatahealth.EMR.pages.Consultation.GeneralAdvicePage;
import com.tatahealth.EMR.pages.Consultation.PrescriptionPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class PrescriptionTest3 {
	@BeforeClass(alwaysRun=true,groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		ConsultationTest currentTest = new ConsultationTest();
		currentTest.loginAndStartConsultation("Prescription 2");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Login_Doctor.driver");
	}
	public static Integer row = 0;
	public static String interactionMedicine = "CROCID ORAL SUSP";
	public static String duplicateMedicine = "";
	public static String templateName = "";
	public static List<String> medicine = new ArrayList<String>();
	public static String cimsMedicineName = "GENTARIL INJ 80mg";
	public static String updateTemplateMedicine = "";
	//public static String templateName = "";
	
	ExtentTest logger;
	
	public synchronized void addMedicine()throws Exception{
		
		logger = Reports.extent.createTest("EMR Add Medicine");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.getAddMedicineButton(Login_Doctor.driver, logger), "Click Add Medicine button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1); 
	}
	
	public synchronized void saveMedicine()throws Exception{
		
		logger = Reports.extent.createTest("EMR Save Medicines  By Right Menu");
	//	FollowUpPage followUp = new FollowUpPage();
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.saveMedicines(Login_Doctor.driver, logger), "Save medicines by right menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1); 
	}
	
	public synchronized void clickAddTemplate()throws Exception{
		
		logger = Reports.extent.createTest("EMR click add as template button");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.getAddTemplateButton(Login_Doctor.driver, logger), "Get Add Template button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		
	}
	
	public synchronized void saveTemplate()throws Exception{
		
		logger = Reports.extent.createTest("EMR click save template button");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.getSaveTemplate(Login_Doctor.driver, logger), "Save medicine template", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}
	
	public synchronized void selectTDHMedicine()throws Exception{
		
		logger = Reports.extent.createTest("EMR Check/Uncheck TDH check box ");
		PrescriptionPage prescription = new PrescriptionPage();
		System.out.println("select tdh catalog");
	//	moveToPrescriptionModule();
		
		Web_GeneralFunctions.click(prescription.getTDHCheckBox(Login_Doctor.driver, logger),"Check/Uncheck TDH medicine", Login_Doctor.driver, logger);
		Thread.sleep(2000);
	}
	
	public synchronized void selectCIMSMedicine()throws Exception{
		
		logger = Reports.extent.createTest("EMR Check/Uncheck CIMS check box ");
		PrescriptionPage prescription = new PrescriptionPage();
		System.out.println("select cims medicine");
		moveToPrescriptionModule();
		
		Web_GeneralFunctions.click(prescription.getCIMSCheckBox(Login_Doctor.driver, logger),"Check/Uncheck CIMS medicine", Login_Doctor.driver, logger);
		Thread.sleep(2000);
	}
	
	
	public synchronized void selectClinicSpecificMedicine()throws Exception{
		
		logger = Reports.extent.createTest("EMR Check/Uncheck clinic specific check box ");
		PrescriptionPage prescription = new PrescriptionPage();
		System.out.println("select clinic specific medicine");
	    moveToPrescriptionModule();
		
		Web_GeneralFunctions.click(prescription.getClinicSpecificCheckBox(Login_Doctor.driver, logger),"Check/Uncheck clinic specific medicine", Login_Doctor.driver, logger);
		Thread.sleep(2000);
	}
	
	public synchronized void clickCloseButtonInTemplate()throws Exception{
		
		logger = Reports.extent.createTest("EMR Close button in template");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.getCloseTemplateButton(Login_Doctor.driver, logger), "Get close button in template modal", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}

      //clicking the prescription button right side of the consultation page 
	@Test(groups= {"Regression","Login"},priority=754)
	public synchronized void moveToPrescriptionModule()throws Exception{
		
		logger = Reports.extent.createTest("EMR move to prescription module");
		DiagnosisPage diagnosisPrescription = new DiagnosisPage();
		
		
		Web_GeneralFunctions.click(diagnosisPrescription.moveToDiagnosisModule(Login_Doctor.driver, logger), "Click Diagnosis/prescription menu", Login_Doctor.driver, logger);
		Thread.sleep(100);
		
		System.out.println("moveToPrescriptionModule 1 done");
	}
	
	
	//Failing because of manual Issue
	//@Test(groups= {"Regression","Login"},priority=755)

	public synchronized void drugToDrugInteraction()throws Exception{
		  
		  logger = Reports.extent.createTest("EMR drug to drug interaction veriication pop up");
		  PrescriptionPage prescription = new PrescriptionPage(); 
		 // selectCIMSMedicine();
		
		selectClinicSpecificMedicine(); selectTDHMedicine();
		Web_GeneralFunctions.click(prescription.getFirstMedicineFromDropDown(row,cimsMedicineName, Login_Doctor.driver, logger),"Get 1st medicine",Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);
		
		    addMedicine();
		  System.out.println("interaction medicine : "+interactionMedicine); 
		  row++;
		  
		  Web_GeneralFunctions.click(prescription.getFirstMedicineFromDropDown(row,interactionMedicine, Login_Doctor.driver, logger), "Get 1st medicine",Login_Doctor.driver, logger); 
		  Web_GeneralFunctions.wait(6);  
		  String text = Web_GeneralFunctions.getText(prescription.getInteractionDialogBox(Login_Doctor.driver, logger), "Get negative interation dialog box",
		  Login_Doctor.driver, logger); 
		  Web_GeneralFunctions.wait(1); 
		  System.out.println("interaction text : "+text + "has negative interactions");
		  if(text.contains("has negative interactions")) { assertTrue(true); }
		  else {
		  assertTrue(false); } Web_GeneralFunctions.wait(1); 
		  System.out.println("drugToDrugInteraction done 18-failed");
		  
		  }
	
	//Failing because of manual Issue
	 //@Test(groups = { "Regression", "Login" }, priority = 756) 
	 public synchronized void clickNOdrugToDrugInteractionPopUp() throws Exception {
	  
	  logger = Reports.extent.
	  createTest("EMR click NO button in drug to drug interaction veriication pop up"); 
	  PrescriptionPage prescription = new PrescriptionPage();
	  
	  Web_GeneralFunctions.click(prescription.getNOInInteractionPopUp(Login_Doctor.driver, logger), "click No button in interaction pop up",
	  Login_Doctor.driver, logger); Thread.sleep(100);
	  Web_GeneralFunctions.click(prescription.getDeleteRow(row,Login_Doctor.driver, logger), "delete row", Login_Doctor.driver, logger);
	  Thread.sleep(200);
	  //saveMedicine(); moveToPrescriptionModule(); 
	  String text =Web_GeneralFunctions.getAttribute(prescription.getDrugName(row,Login_Doctor.driver, logger), "value", "Get drug name value",Login_Doctor.driver, logger);
	  Thread.sleep(500); if (text.isEmpty()) {
	  assertTrue(true); } else { assertTrue(false); } Thread.sleep(100); 
	   

	  }
	 
	//Failing because of manual Issue
	// @Test(groups= {"Regression","Login"},priority=757) 
	  public synchronized void clickYesdrugToDrugInteractionPopUp()throws Exception{
	  
	  row++; //drugToDrugInteraction(); //selectTDHMedicine();
	  //selectCIMSMedicine(); 
	  moveToPrescriptionModule();
	  addMedicine(); 
	  logger = Reports.extent.createTest("EMR click yes button in drug to drug interaction veriication pop up"); 
	  PrescriptionPage prescription = new PrescriptionPage();
	  System.out.println("interaction medicine in yes : "+interactionMedicine);
	  Web_GeneralFunctions.click(prescription.getFirstMedicineFromDropDown(row,interactionMedicine, Login_Doctor.driver, logger), "Get 1st medicine",Login_Doctor.driver, logger); 
	  Web_GeneralFunctions.wait(1);
	  Web_GeneralFunctions.click(prescription.getInteractionDialogBox(Login_Doctor.driver, logger), "Get Interaction dialog box", Login_Doctor.driver, logger);
	  Web_GeneralFunctions.wait(2);
	  Web_GeneralFunctions.click(prescription.getYesInInteractionPopUp(Login_Doctor
	  .driver, logger), "click Yes button in interaction pop up",
	  Login_Doctor.driver, logger);   Web_GeneralFunctions.wait(1);
 	  //Thread.sleep(1000); 
	 // saveMedicine(); 
	  //moveToPrescriptionModule(); 
	  String text= Web_GeneralFunctions.getAttribute(prescription.getDrugName(row,
	  Login_Doctor.driver, logger), "value", "Get drug name value",
	  Login_Doctor.driver, logger); 
	  Web_GeneralFunctions.wait(1);
	  if(text.equalsIgnoreCase(interactionMedicine)) { assertTrue(true); }else {
	  assertTrue(false); } }
	 
	 @Test(groups= {"Regression","Login"},priority=758)
		public synchronized void duplicateMedicine()throws Exception{
			
			logger = Reports.extent.createTest("EMR duplicate medicines");
			PrescriptionPage prescription = new PrescriptionPage();
		//	saveMedicine();
			addMedicine(); 

			row++;
		//	moveToPrescriptionModule();
			duplicateMedicine = Web_GeneralFunctions.getAttribute(prescription.getDrugName(0, Login_Doctor.driver, logger), "value", "Get 1st prescribed drug name", Login_Doctor.driver, logger);
			//System.out.println("text duplicate text : "+duplicateMedicine);
			
			Web_GeneralFunctions.sendkeys(prescription.getDrugName(row, Login_Doctor.driver, logger), duplicateMedicine, "Sending duplicate values in drug name field", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(2);
			String text = Web_GeneralFunctions.getText(prescription.getDuplicateBox(Login_Doctor.driver, logger), "Get duplicate box", Login_Doctor.driver, logger);
			//System.out.println("duplicate medicine : "+text);
			Web_GeneralFunctions.wait(2);
			if(text.contains("Duplicate Medicine Found")) {
				assertTrue(true);
			}else {
				assertTrue(false);
			}
			System.out.println("saveSuccessfully done 25");

		}

		@Test(groups= {"Regression","Login"},priority=759)
		public synchronized void clickNODuplicateBoxPopUp()throws Exception{
			
			logger = Reports.extent.createTest("EMR click NO button in duplicate medicine pop up");
			PrescriptionPage prescription = new PrescriptionPage();
			
			Web_GeneralFunctions.click(prescription.getNOButtonInDuplicateBox(Login_Doctor.driver, logger), "click No button in duplicate medicine pop up", Login_Doctor.driver, logger);
			  Web_GeneralFunctions.wait(1);
 			//saveMedicine();
			//moveToPrescriptionModule();
			String text = Web_GeneralFunctions.getAttribute(prescription.getDrugName(row, Login_Doctor.driver, logger), "value", "Get drug name value", Login_Doctor.driver, logger);
			  Web_GeneralFunctions.wait(1);
			if(text.isEmpty()) {
				assertTrue(true);
			}else {
				assertTrue(false);
			}
			Web_GeneralFunctions.click(prescription.getDeleteRow(row, Login_Doctor.driver, logger),"delete row", Login_Doctor.driver, logger);

			System.out.println("clickNODuplicateBoxPopUp done 26");

		}
		
		@Test(groups= {"Regression","Login"},priority=760)
		public synchronized void clickYesDuplicateMedicinePopUp()throws Exception{
			
  			logger = Reports.extent.createTest("EMR click yes button in duplicate medicine pop up");
			PrescriptionPage prescription = new PrescriptionPage();
			addMedicine();
			row++;

			Web_GeneralFunctions.sendkeys(prescription.getDrugName(row, Login_Doctor.driver, logger), duplicateMedicine, "Sending duplicate values in drug name field", Login_Doctor.driver, logger);
			String yes = Web_GeneralFunctions.getText(prescription.getYesButtonInDuplicateBox(Login_Doctor.driver, logger), "get yes value", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(prescription.getYesButtonInDuplicateBox(Login_Doctor.driver, logger), "click Yes button in duplicate medicine pop up", Login_Doctor.driver, logger);
			Thread.sleep(500);
			Robot robot = new Robot();   	
			   robot.keyPress(KeyEvent.VK_ENTER);   
		       robot.keyRelease(KeyEvent.VK_ENTER);
			String text = Web_GeneralFunctions.getAttribute(prescription.getDrugName(row, Login_Doctor.driver, logger), "value", "Get drug name value", Login_Doctor.driver, logger);
			Thread.sleep(500);
			if(text.equalsIgnoreCase(duplicateMedicine)) {
				assertTrue(true);
			}else {
				assertTrue(false);
			}
		}
	 
}

